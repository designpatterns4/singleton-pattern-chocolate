package com.singleton.threadsafe;

public class Singleton {
    private static Singleton uniqueInstance;

    //other usefull instance variables here
    private Singleton() {}
    public static synchronized Singleton getInstance() {
        if(uniqueInstance == null) {
            uniqueInstance = new Singleton();
        }
        return uniqueInstance;
    }

    //other usefull methods here
    public String getDescription() {
        return "I'm a thread safe Singleton";
    }
}
