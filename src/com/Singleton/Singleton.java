package com.Singleton;

//This is not thread safe!!
public class Singleton {
    private static Singleton uniqueInstance;
    private Singleton() {}
    public static Singleton getInstance() {
        if(uniqueInstance == null) {
            uniqueInstance = new Singleton();
        }
        return uniqueInstance;
    }
    //other usefull methods here
    public String getDescription() {
        return "I'm a classic Singleton!";
    }
}
